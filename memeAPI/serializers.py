from memeAPI.models import Meme
from rest_framework import serializers


class MemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meme
        fields = ['id', 'type', 'meme', 'annotation',
                  'upload_date', 'uploader_name', 'hash']
