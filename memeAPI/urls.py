from django.urls import path, include
from memeAPI import views

app_name = "memeAPI"
urlpatterns = [
    path("images/<int:pk>", views.ImageMemes.as_view()),
    path("videos/<int:pk>", views.VideoMemes.as_view()),
    path("audios/<int:pk>", views.AudioMemes.as_view()),
    path("images/random", views.RandomImage.as_view()),
]

urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
]
