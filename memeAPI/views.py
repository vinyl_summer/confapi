from rest_framework.response import Response
from rest_framework import permissions, authentication
from rest_framework.views import APIView
from memeAPI.models import Meme
from memeAPI.serializers import MemeSerializer
from rest_framework import generics
import random


class ImageMemes(generics.RetrieveUpdateDestroyAPIView):
    queryset = Meme.objects.filter(type="Image")
    serializer_class = MemeSerializer
    permission_classes = [permissions.IsAuthenticated]


class VideoMemes(generics.RetrieveUpdateDestroyAPIView):
    queryset = Meme.objects.filter(type="Video")
    serializer_class = MemeSerializer
    permission_classes = [permissions.IsAuthenticated]


class AudioMemes(generics.RetrieveUpdateDestroyAPIView):
    queryset = Meme.objects.filter(type="Audio")
    serializer_class = MemeSerializer
    permission_classes = [permissions.IsAuthenticated]


class RandomImage(APIView):
    authentication_classes = [authentication.BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        random_meme = random.choice(list(Meme.objects.filter(type='Image')))
        return Response(random_meme.annotation)
