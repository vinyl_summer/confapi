from django.db import models

MEME_TYPES = ['Image', 'Video', 'Voice']
MEME_TYPES = [(item, item) for item in MEME_TYPES]


class Meme(models.Model):
    meme = models.FileField()
    type = models.CharField(choices=MEME_TYPES, max_length=50)
    annotation = models.CharField(max_length=500)
    upload_date = models.DateTimeField()
    uploader_name = models.CharField(max_length=100)
    hash = models.TextField()

    def __str__(self):
        return self.type
